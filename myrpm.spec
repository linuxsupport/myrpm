Name: myrpm
Version: 1.1
Release: 36%{?dist}
Summary: Super useful program

Group: CERN/Utilities
License: CERN
URL: 	https://cern.ch/myrpm
Source: %{name}-%{version}.tgz

BuildRequires: gcc-c++

%description
Just try to live without this

%prep
%setup -n %{name}-%{version}


%build
rm -rf %{buildroot}
cd src/
g++ -Wall -g -o hello-world hello-world.cpp


%install
install -d %{buildroot}%{_bindir}
cd src/
install -m 0755 hello-world %{buildroot}%{_bindir}/hello-world

%post
/bin/false

%files
%defattr(-,root,root,-)
%{_bindir}/hello-world
%doc src/README.md


%changelog
* Thu Jul 04 2024 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 1.1-34
- Bump

* Thu Jul 04 2024 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 1.1-33
- Build for rhel8

* Thu Nov 02 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-32
- Bump

* Mon Oct 23 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 1.1-30
- Remove CS8 build

* Mon Jan 13 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-3
- Version bump

* Mon Nov 25 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-2
- Version bump

* Fri Oct 11 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.0-1
- Initial version
