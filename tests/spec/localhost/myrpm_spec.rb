require 'spec_helper'

describe package('myrpm') do
  it { should be_installed }
end

describe file('/usr/bin/hello-world') do
  it { should exist }
  it { should be_file }
  it { should be_executable }
  it { should be_owned_by 'root' }
  it { should be_mode 755 }
end

describe command('/usr/bin/hello-world') do
  its(:stdout) { should match /Hello, World!/ }
end

describe command('/usr/bin/hello-world handsome') do
  its(:stdout) { should match /Hello, handsome!/ }
end
